import React from 'react';
import {Text, Image, View, TextInput, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const DetailScreen = () => {
  const navigation = useNavigation();

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: '600',
          marginTop: 10,
          alignSelf: 'center',
          color: 'black',
          fontFamily: 'rajdhani',
        }}>
        {' '}
        Connexion{' '}
      </Text>

      <Image
        source={require('../Assets/img/tipnetlogo.png')}
        style={{alignSelf: 'center', marginTop: '10%'}}></Image>

      <Text
        style={{
          color: '#4958FF',
          alignSelf: 'center',
          marginTop: '3%',
          fontSize: 20,
          fontWeight: 'bold',
          fontFamily: 'Rajdhani',
        }}>
        {' '}
        Ton partenaire en paris sportifs{' '}
      </Text>

      <View
        style={{
          alignSelf: 'center',
          margin: '5%',
          borderRadius: 10,
          borderColor: 'black',
          borderWidth: 1,
          padding: 10,
          fontFamily: 'Rajdhani',
        }}>
        <Text
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            margin: 3,
            fontFamily: 'Rajdhani',
          }}>
          Email{' '}
        </Text>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          style={{height: 40, width: 300, backgroundColor: 'white'}}
          placeholder="abc@gmail.com"></TextInput>
      </View>

      <View
        style={{
          alignSelf: 'center',
          margin: '5%',
          borderRadius: 10,
          borderColor: 'black',
          borderWidth: 1,
          padding: 10,
        }}>
        <Text
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            margin: 3,
            fontFamily: 'Rajdhani',
          }}>
          Password{' '}
        </Text>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          style={{height: 40, width: 300, backgroundColor: 'white'}}
          placeholder="123456"></TextInput>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('ResetPass')}
        style={{
          height: '8%',
          width: '80%',
          backgroundColor: '#4C2E84',
          alignSelf: 'center',
          borderRadius: 20,
          marginTop: 20,
        }}>
        <Text
          style={{
            fontSize: 20,
            padding: 15,
            color: 'white',
            alignSelf: 'center',
            fontFamily: 'Rajdhani',
          }}>
          {' '}
          S'inscrire{' '}
        </Text>
      </TouchableOpacity>

      <View
        style={{
          flexDirection: 'row',
          height: '10%',
          width: '100%',
          marginRight: 20,
          marginTop: 30,
        }}>
        <View style={{flex: 2.5}}>
          <Image
            source={require('../Assets/img/googlelogo.jpg')}
            style={{
              alignSelf: 'center',
              borderRadius: 10,
              marginTop: '2%',
              height: '80%',
              width: '60%',
            }}></Image>
        </View>
        {/* <Image source={require('../Assets/img/twitterlogo.png')}   style={{ alignSelf:"center" , marginTop:"20%" }}></Image> */}
        <View style={{flex: 2.5}}>
          <Image
            source={require('../Assets/img/instagramlogo.png')}
            style={{
              alignSelf: 'center',
              borderRadius: 10,
              marginTop: '2%',
              height: '80%',
              width: '60%',
            }}></Image>
        </View>
        <View style={{flex: 2.5, margin: 2}}>
          <Image
            source={require('../Assets/img/twitter.jpg')}
            style={{
              alignSelf: 'center',
              borderRadius: 10,
              marginTop: '2%',
              height: '80%',
              width: '60%',
            }}></Image>
        </View>
        <View style={{flex: 2.5}}>
          <Image
            source={require('../Assets/img/facebook.png')}
            style={{
              alignSelf: 'center',
              borderRadius: 10,
              marginTop: '2%',
              height: '80%',
              width: '60%',
            }}></Image>
        </View>
      </View>

      <View style={{flexDirection: 'row', marginTop: 20}}>
        <Text
          style={{
            color: 'black',
            marginLeft: 40,
            fontSize: 16,
            fontFamily: 'Rajdhani',
          }}>
          {' '}
          Vous avez déja un compte ?{' '}
        </Text>
        <Text
          style={{
            color: 'blue',
            fontSize: 16,
            fontWeight: 'bold',
            fontFamily: 'Rajdhani',
          }}>
          {' '}
          Se Connecter{' '}
        </Text>
      </View>
    </View>
  );
};

export default DetailScreen;
