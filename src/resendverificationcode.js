import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const Resendverificationcode = (props) => {




  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <Image
        source={require('../Assets/img/Tipnet.png')}
        style={{alignSelf: 'center', marginTop: hp("30%")}}></Image>

      <Card
        style={{
          borderRadius: 30,
          height: '55%',
          width: '100%',
          alignSelf: 'center',
          marginTop: hp("30%"),
          backgroundColor: '#4958FF',
        }}>
        <Text
          style={{
            fontSize: hp('2.3%'),
            color: 'white',
            marginTop: hp('5%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          MOT DE PASSE OUBLIÉ
        </Text>

        <Text
          style={{
            fontSize: hp('2.3%'),
            color: 'white',
            marginTop: hp('3%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Indiquez l'adresse électronique du {'\n'} compte pour lequel vous
          souhaitez {'\n'}
          réinitialiser votre mot de passe.
        </Text>

     


        <TouchableOpacity
           onPress={() => props.navigation.navigate('Otpinput')}
          style={{
            height: '12%',
            width: '80%',

            alignSelf: 'center',
            marginTop: hp('3%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: 15,
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight: 'bold',
            }}>
            {' '}
            Renvoyer le code de vérification
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}
          style={{
            height: '12%',
            width: '80%',
            alignSelf: 'center',
        
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: 15,
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight: 'bold',
            }}>
            {' '}
            Modifier l'adresse e-mail
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  );
};

export default Resendverificationcode;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('8%'),
    height: '20%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: '#21899C',
  },
});
