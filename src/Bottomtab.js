import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SettingsScreen from './BottomTabscreen/Setting';
import * as React from 'react';
import Myprofile from './BottomTabscreen/Myprofile';
import Mypro from './BottomTabscreen/mypro';
import Databse from './BottomTabscreen/database';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Tab = createBottomTabNavigator();

function BottomTab() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="myprofile"
       component={Myprofile}
        options={{
          headerShown: false,
          tabBarLabel: 'Setting',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" size={30} color="blue" />
          ),
        }}></Tab.Screen>
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Setting',
          tabBarIcon: ({color, size}) => (
            <Feather name="list" size={30} color="blue" />
          ),
        }}
      />
      <Tab.Screen
        name="mypro"
        component={Mypro}
        options={{
          headerShown: false,
          tabBarLabel: 'Setting',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="bar-chart" size={30} color="blue" />
          ),
        }}
      />
      <Tab.Screen
        name="Databse"
        component={Databse}
        options={{
          headerShown: false,
          tabBarLabel: 'Setting',
          tabBarIcon: ({color, size}) => (
            <FontAwesome name="user-circle-o" size={30} color="blue" />
          ),
        }}
      />
    </Tab.Navigator>

    
  );
}

export default BottomTab;
