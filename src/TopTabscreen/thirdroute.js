import React, {useState} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Card} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';


const thirdhRoute = () => {
  const [tableHead, settableHead] = useState([
    '',
    'cote moy',
    'Nibre de tips ',
    '% de win',
    
  ]);
  const [tableTitle, settableTitle] = useState( 
    ['Football ', 'Tennis', 'Basket' , ]);


  const [tableData, settableData] = useState([
    ['', '', ''],
    ['', '', ''],
  
    
  ]);

  return (
    <ScrollView style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F3F3F3',
        }}>
        <View
          style={{height: heightPercentageToDP('10%'), flexDirection: 'row'}}>
          <View style={{flex: 3}}>
            <Text style={styles.Text1}>140</Text>
            <Text style={styles.Text11}>Nbre de</Text>
            <Text style={styles.Text11}>tips</Text>
          </View>

          <View style={{flex: 3}}>
            <Text style={styles.Text1}>2.30</Text>
            <Text style={styles.Text11}>cote</Text>
            <Text style={styles.Text11}>moy</Text>
          </View>

          <View style={{flex: 3}}>
            <Text style={styles.Text1}>54%</Text>
            <Text style={styles.Text11}>% de</Text>
            <Text style={styles.Text11}>win</Text>
          </View>
        </View>

        <View
          style={{
            height: heightPercentageToDP('15%'),
            flexDirection: 'row',
            marginTop: heightPercentageToDP('2%'),
          }}>
          <View style={{flex: 3}}>
            <Text style={styles.Text1}>343%</Text>
            <Text style={styles.Text11}>ROI</Text>
          </View>

          <View style={{flex: 3}}>
            <Text style={styles.Text1}>343%</Text>
            <Text style={styles.Text11}>ROC</Text>
          </View>

          <View style={{flex: 3}}>
            <Text style={styles.Text1}>54%</Text>
            <Text style={styles.Text11}>Capital de</Text>
            <Text style={styles.Text11}> depart</Text>
          </View>
        </View>

        <Text style={styles.Text2}>Evolution de la bankroll </Text>

        <Image
          source={require('../../Assets/img/graph.png')}
          style={styles.graph}></Image>
      </View>


<View style={{margin:heightPercentageToDP("3%") }}>  
      <Table borderStyle={{borderWidth: 2 , }}>
        <Row
          data={tableHead}
          flexArr={[1, 2, 1, 1]}
          style={styles.head}
          textStyle={styles.text}
        />
        <TableWrapper style={styles.wrapper}>
          <Col
            data={tableTitle}
            style={styles.title}
            heightArr={[28, 28]}
            textStyle={styles.text}
          />
          <Rows
            data={tableData}
            flexArr={[2, 1, 1]}
            style={styles.row}
            textStyle={styles.text}
          />
        </TableWrapper>
      </Table>
      </View>
      {/* <Pie
              radius={80}
              sections={[
                {
                  percentage: 10,
                  color: '#C70039',
                },
                {
                  percentage: 20,
                  color: '#44CD40',
                },
                {
                  percentage: 30,
                  color: '#404FCD',
                },
                {
                  percentage: 40,
                  color: '#EBD22F',
                },
              ]}
              strokeCap={'butt'}
            />
      */}
    </ScrollView>
  );
};
export default thirdhRoute;

const styles = StyleSheet.create({
  Text1: {
    alignSelf: 'center',
    marginTop: heightPercentageToDP('4%'),
    fontSize: heightPercentageToDP('2.8%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  Text11: {
    alignSelf: 'center',
    // marginTop: heightPercentageToDP('0.5%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: '400',
    color: 'gray',
  },

  Text2: {
    fontSize: heightPercentageToDP('2.2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
    alignSelf: 'flex-start',
    margin: heightPercentageToDP('2%'),
    marginLeft: widthPercentageToDP('10%'),
  },

  graph: {
    borderRadius: 20,
    marginBottom: heightPercentageToDP('3%'),
  },

  head: {height: 40, backgroundColor: '#f1f8ff'},
  wrapper: {flexDirection: 'row'},
  title: {flex: 1, backgroundColor: '#f6f8fa'},
  row: {height: 28},
  text: {textAlign: 'center'},
});
