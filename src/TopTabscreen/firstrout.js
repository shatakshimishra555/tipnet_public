import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Card} from 'react-native-paper';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {heightPercentageToDP} from 'react-native-responsive-screen';

const FirstRoute = () => {
  return (
    <View
      style={{
        flex: 1,
      }}>
      <ScrollView>
        <Card style={styles.cardview1}>
          <Text style={styles.text22}>
            Je m’appelle Curtis Prono, je suis pronostiqueur depuis 3 maintenant
            et possède plusisuers bankrolls a plus de 50% de win
           
          </Text>
        </Card>

        <Card style={styles.cardview}>
          <Text style={styles.text1}> Twitter </Text>
          <Text style={styles.text2}> Lien Twitter </Text>
        </Card>

        <Card style={styles.cardview}>
          <Text style={styles.text1}> Facebook </Text>
          <Text style={styles.text2}> Lien Facebook </Text>
        </Card>

        <Card style={styles.cardview}>
          <Text style={styles.text1}> Instagram </Text>
          <Text style={styles.text2}> Lien Instagram </Text>
        </Card>

        <Card style={styles.cardview}>
          <Text style={styles.text1}> Twitter </Text>
          <Text style={styles.text2}> Lien Twitter </Text>
        </Card>
      </ScrollView>
    </View>
  );
};
export default FirstRoute;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '15%',
    width: '70%',
  },

  cardview: {
    height: heightPercentageToDP('10%'),
    width: widthPercentageToDP('90%'),
    marginTop: heightPercentageToDP('2%'),
    backgroundColor: '#F3F3F3',
    alignSelf: 'center',
    borderRadius: 20,
    marginBottom:heightPercentageToDP("2%")
  },

  cardview1: {
    height: heightPercentageToDP('15%'),
    width: widthPercentageToDP('90%'),
    marginTop: heightPercentageToDP('2%'),
    backgroundColor: '#F3F3F3',
    alignSelf: 'center',
    borderRadius: 20,
  },

  text1: {
    color: 'gray',
    fontWeight: 'bold',
    padding: heightPercentageToDP('0.5%'),
    alignSelf: 'flex-start',
    fontSize: heightPercentageToDP('2.3%'),
  },

  text2: {
    padding: heightPercentageToDP('1%'),
    color: 'black',
    alignSelf: 'flex-start',
    fontSize: heightPercentageToDP('2.3%'),
  },

  
  text22: {
    padding: heightPercentageToDP('2%'),
    color: 'black',
    alignSelf: 'flex-start',
    fontSize: heightPercentageToDP('2.3%'),
  },
});
