

import React, {useState} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Card} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';

const fourthRoute = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F3F3F3',
      }}>
      <ScrollView>
        <Card style={styles.cardview}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2.8, justifyContent: 'center'}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.profilepic}></Image>
            </View>
            <View style={{flex: 3}}>
              <Text style={styles.text7}>Test Test2</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={styles.text9}>Abonne</Text>
            </View>

            <View style={{flex: 2}}>
              <View
                style={{
                  backgroundColor: '#00CADB',
                  margin: widthPercentageToDP('3%'),
                  marginTop: heightPercentageToDP('3%'),
                  borderRadius: 5,
                  elevation: 10,
                }}>
                <TouchableOpacity>
                  <AntDesign
                    name="adduser"
                    size={30}
                    color="white"
                    style={styles.adduser}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Card>

        <Card style={styles.cardview}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2.8, justifyContent: 'center'}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.profilepic}></Image>
            </View>
            <View style={{flex: 3}}>
              <Text style={styles.text7}>Test Test2</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={styles.text9}>Follower </Text>
            </View>

            <View style={{flex: 2}}>
              <View
                style={{
                  backgroundColor: '#00EE6D',
                  margin: widthPercentageToDP('3%'),
                  marginTop: heightPercentageToDP('3%'),
                  borderRadius: 5,
                  elevation: 10,
                }}>
                <TouchableOpacity>
                  <AntDesign
                    name="deleteuser"
                    size={30}
                    color="white"
                    style={styles.deleteuser}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Card>

        <Card style={styles.cardview}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2.8, justifyContent: 'center'}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.profilepic}></Image>
            </View>
            <View style={{flex: 3}}>
              <Text style={styles.text7}>Test Test2</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={styles.text9}>Abonne</Text>
            </View>

            <View style={{flex: 2}}>
              <View
                style={{
                  backgroundColor: '#00CADB',
                  margin: widthPercentageToDP('3%'),
                  marginTop: heightPercentageToDP('3%'),
                  borderRadius: 5,
                  elevation: 10,
                }}>
                <TouchableOpacity>
                  <AntDesign
                    name="adduser"
                    size={30}
                    color="white"
                    style={styles.adduser}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Card>

        <Card style={styles.cardview}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2.8, justifyContent: 'center'}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.profilepic}></Image>
            </View>
            <View style={{flex: 3}}>
              <Text style={styles.text7}>Test Test2</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={styles.text9}>Follower </Text>
            </View>

            <View style={{flex: 2}}>
              <View
                style={{
                  backgroundColor: '#00EE6D',
                  margin: widthPercentageToDP('3%'),
                  marginTop: heightPercentageToDP('3%'),
                  borderRadius: 5,
                  elevation: 10,
                }}>
                <TouchableOpacity>
                  <AntDesign
                    name="deleteuser"
                    size={30}
                    color="white"
                    style={styles.deleteuser}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Card>
      </ScrollView>
    </View>
  );
};
export default fourthRoute;

const styles = StyleSheet.create({
  profilepic: {
    alignSelf: 'center',
    margin: heightPercentageToDP('2%'),
    height: '70%',
    width: '70%',
    borderRadius: 20,
    marginLeft: -widthPercentageToDP('2%'),
  },
  text7: {
    marginTop: heightPercentageToDP('4%'),
    fontSize: heightPercentageToDP('2.3%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  text9: {
    marginTop: heightPercentageToDP('4%'),
    fontSize: heightPercentageToDP('2.3%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: '#00CADB',
    textAlign: 'center',
  },

  cardview: {
    flex: 1,
    margin: heightPercentageToDP('2%'),
    height: heightPercentageToDP('12%'),
    width: widthPercentageToDP('90%'),
    alignSelf: 'center',
    borderRadius: 20,
  },

  adduser: {
    alignSelf: 'center',
    elevation: 30,
    padding: heightPercentageToDP('0.5%'),
  },

  deleteuser: {
    alignSelf: 'center',
    elevation: 30,
    padding: heightPercentageToDP('0.5%'),
  },
});
