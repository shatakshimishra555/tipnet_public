import React, {useState} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Sliderscreen2 = () => {
  const navigation = useNavigation();

  const [formdata, setFormdata] = useState({name:''});
  const [error, setError] = useState({});

  const registerapi = () => {
    let data = {name: Firstname};
    return fetch('https://promotebio.com/tipster/api/users/add', data)
      .then(response => response.json())
      .then(json => {
        return json.movies;
      })
      .catch(error => {
        console.error('please enter correct details');
      });
  };

  const handleChange = (e, name) => {
    //let name=e.target.name
    //let value=e.target.value
    console.log('fffff', e);
    //setFormdata()
    // setFormdata(value=>   )
  };

  const submit = () => {
   console.log("sssssssssss",error)
  //  Checkvalidation()
  //   if(error.length>0){
  //    alert("hhhhh")
  //   } else {
  //    alert("yyy")
  //   }
  };

  const changeText = e => {
    let name = e.target._internalFiberInstanceHandleDEV.memoizedProps.name; // fetches name i.e "Testing"
    let value = e.nativeEvent.text;

    console.log('event ', e.nativeEvent, name);
    setFormdata({...formdata,[name]: value});
    //setFormdata(() => ({[Lastname]: value}));
    //setFormdata(() => ({[Refferalcode]: value}));
    console.log('form data', formdata);
  };




  const  Checkvalidation = ()=>{
    if(formdata.name===''){
      setError({...error,[error.name]: "please enter first name " });
    } else if ( formdata.lastname===''  ){
      setError({...error,[error.lastname]: "please enter last name " });
    } else 
    if(formdata.refferal_code===''){
      setError({...error,[error.refferal_code]: "please enter refferal code " });
    }else {
      setError({});
    }
  }

  return (
    <View style={styles.maincontainer}>
      <Image
        source={require('../Assets/img/tipnetlogo.png')}
        style={styles.img1}></Image>

      <Text style={styles.text1}>Votre inscription est réussi </Text>

      <View style={{marginTop: hp('8%')}}>
        <Text style={styles.text2}>Nom</Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            name="first_name"
            onChange={changeText}
            placeholder="Enterz votre nom"></TextInput>
        </View>
        {error.name && <Text>{error.name}</Text>}

        <Text style={styles.text2}>Prenom</Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            name="last_name"
            onChange={changeText}
            placeholder="Enterz votre Prenom"></TextInput>
        </View>
        {error.lastname && <Text>{error.lastname}</Text>}
        <Text style={styles.text2}>Code parrainage</Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            name="Refferal_code"
            onChange={changeText}
            placeholder="Enterz votre parraninge"></TextInput>
        </View>
        {error.refferal_code && <Text>{error.refferal_code}</Text>}
      </View>

      <TouchableOpacity
        // onPress={() => navigation.navigate('Slider3')}
        OnPress={submit}
        onClick={submit}
        style={styles.btn}>
        <Text style={styles.btntxt}> Contineur </Text>
      </TouchableOpacity>

      <Image
        source={require('../Assets/img/appslider1.png')}
        style={styles.img3}></Image>
    </View>
  );
};

export default Sliderscreen2;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '15%',
    width: '70%',
  },

  img3: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '5%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('2%'),
    alignSelf: 'center',
    color: '#000000',
    fontFamily: 'Rajdhani',
  },

  text2: {
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Rajdhani',
    marginLeft: wp('10%'),
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  btn: {
    height: '8%',
    width: '80%',
    backgroundColor: '#4C2E84',
    alignSelf: 'center',
    borderRadius: 20,
    marginTop: 20,
  },
  btntxt: {
    fontSize: 20,
    padding: 15,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'Rajdhani',
  },

  inputbox: {
    alignSelf: 'center',
    margin: '5%',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    padding: 8,
    fontFamily: 'Rajdhani',
  },

  textinputbox: {height: 40, width: 300, backgroundColor: 'white'},
});
