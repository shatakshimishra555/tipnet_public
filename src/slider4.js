import React from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Sliderscreen4 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.maincontainer}>
      <Image
        source={require('../Assets/img/tipnetlogo.png')}
        style={styles.img1}></Image>

      <Text style={styles.text1}>Votre inscription est réussi </Text>

      <View style={{marginTop: hp('8%')}}>
        <Text style={styles.text2}>IBAN</Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            placeholder="Enterz votre IBAN "></TextInput>
        </View>

        <Text style={styles.text2}> BIC </Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            placeholder="Enterz votre BIC"></TextInput>
        </View>

        <Text style={styles.text2}>Numéro de téléphone</Text>

        <View style={styles.inputbox}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textinputbox}
            placeholder="Numéro de téléphone "></TextInput>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate('Slider5')}
        style={styles.btn}>
        <Text style={styles.btntxt}> Validater </Text>
      </TouchableOpacity>

      <Image
        source={require('../Assets/img/appslider1.png')}
        style={styles.img3}></Image>
    </View>
  );
};

export default Sliderscreen4;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '15%',
    width: '70%',
  },

  img3: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '5%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('2%'),
    alignSelf: 'center',
    color: '#000000',
    fontFamily: 'Rajdhani',
  },

  text2: {
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Rajdhani',
    marginLeft: wp('10%'),
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  btn: {
    height: '8%',
    width: '80%',
    backgroundColor: '#4C2E84',
    alignSelf: 'center',
    borderRadius: 20,
    marginTop: 20,
  },
  btntxt: {
    fontSize: 20,
    padding: 15,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'Rajdhani',
  },

  inputbox: {
    alignSelf: 'center',
    margin: '5%',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    padding: 8,
    fontFamily: 'Rajdhani',
  },

  textinputbox: {height: 40, width: 300, backgroundColor: 'white'},
});
