import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const ResetPassword = props => {
  const [data, setData] = React.useState({
    email: '',
    isValidUser: true,
    check_textInputChange: false,
  });
  const [error, setError] = React.useState({error: false, errorMsg: ''});

  const textInputChange = val => {
    if (val.trim().length >= 6) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const handleValidUser = val => {
    if (val.trim().length >= 6) {
      setData({
        ...data,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  const loginHandle = email => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (data.email.length == 0) {
      //Alert.alert( 'Please enter your registerd Email Id' );
      setError({
        ...error,
        error: true,
        errorMsg: 'Please enter your registerd Email Id',
      });
      return;
    } else if (reg.test(email) === false) {
      setError({
        ...error,
        error: true,
        errorMsg: 'Invail email. Please enter your registered email',
      });
      return;
    } else {
      props.navigation.navigate('resendverificationcode');
    }
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <Image
        source={require('../Assets/img/Tipnet.png')}
        style={{alignSelf: 'center', marginTop: '40%'}}></Image>

      <Card
        style={{
          borderRadius: 30,
          height: '70%',
          width: '100%',
          alignSelf: 'center',
          marginTop: '15%',
          backgroundColor: '#4958FF',
        }}>
        <Text
          style={{
            fontSize: hp('2.3%'),
            color: 'white',
            marginTop: hp('5%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          MOT DE PASSE OUBLIÉ
        </Text>

        <Text
          style={{
            fontSize: hp('2.3%'),
            color: 'white',
            marginTop: hp('3%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Indiquez l'adresse électronique du {'\n'} compte pour lequel vous
          souhaitez {'\n'}
          réinitialiser votre mot de passe.
        </Text>

        <Text
          style={{
            fontSize: hp('2.5%'),
            color: 'white',
            marginTop: hp('5%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Email{' '}
        </Text>

        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          type="email"
          onChangeText={val => textInputChange(val)}
          onEndEditing={e => handleValidUser(e.nativeEvent.text)}
          style={{
            height: '12%',
            width: '80%',
            backgroundColor: 'white',
            marginTop: hp('2%'),
            marginLeft: hp('5%'),
          }}
          placeholder="abc@gmail.com"
        />

        {error.error && (
          <View>
            <Text style={{color: 'red', marginLeft: '10%'}}>
              {error.errorMsg}
            </Text>
          </View>
        )}

        {/* {data.email == 0 ? null : (
          <View>
            <Text style={{color: 'white', marginLeft: '10%'}}>
              Please enter your registerd email.
            </Text>
          </View>
        )} */}

        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}
          onPress={() => {
            loginHandle(data.email);
          }}
          style={{
            height: '12%',
            width: '80%',
            backgroundColor: '#00EE6D',
            alignSelf: 'center',
            marginTop: hp('4%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: hp('2%'),
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
            }}>
            {' '}
            Request reset password link
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}
          style={{
            height: '12%',
            width: '80%',

            alignSelf: 'center',
            marginTop: hp('5%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: 15,
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight: 'bold',
            }}>
            {' '}
            Cancel
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  );
};

export default ResetPassword;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('8%'),
    height: '20%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: '#21899C',
  },
});
