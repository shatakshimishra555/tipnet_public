import React from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Sliderscreen1 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.maincontainer}>
      <Image
        source={require('../Assets/img/tipnetlogo.png')}
        style={styles.img1}></Image>

      <Text style={styles.text1}>Connectez-vous pour explorer</Text>

      <View style={styles.btncontainer}>
        <View style={styles.btnview}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.btntxt}> Tipster</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.btnview}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.btntxt}> Follower</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('Slider2')}>
        <Image
          source={require('../Assets/img/appslider1.png')}
          style={styles.img2}></Image>
      </TouchableOpacity>
    </View>
  );
};

export default Sliderscreen1;

const styles = StyleSheet.create({
  btn: {
    height: '100%',
    width: '80%',
    backgroundColor: 'white',
    alignSelf: 'center',
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 20,
  },

  btntxt: {
    alignSelf: 'center',
    padding: 10,
    fontWeight: 'bold',
    fontSize: 15,
  },

  btnview: {flex: 5, alignContent: 'center'},

  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: '90%',
    height: '5%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('5%'),
    alignSelf: 'center',
    color: '#000000',
    fontFamily: 'Rajdhani',
  },

  btncontainer: {
    height: '6%',
    width: '100%',
    flexDirection: 'row',
    marginTop: hp('5%'),
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },
});
