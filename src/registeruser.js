import React, {useState} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Sliderscreen1  from "./slider1"
import Sliderscreen2  from "./slider2"
import Sliderscreen3  from "./slider3"
import Sliderscreen4  from "./slider4"

const RegisterUser = () => {
  const navigation = useNavigation();

  const [step, setStep] = useState(0);
  const [formdata, setFormdata] = useState({});

  


  submit()
  {




  }

  return (
    <View style={styles.maincontainer}>
      {step===0 && (
        <Sliderscreen1 action={submit}/>
      )}
      {step===1 && (
        <Sliderscreen2 />
      )}
      {step===2 && (
        <Sliderscreen3 />
      )}
    </View>
  );
};

export default RegisterUser;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '15%',
    width: '70%',
  },

  img3: {
    alignSelf: 'center',
    marginTop: '10%',
    height: '5%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('2%'),
    alignSelf: 'center',
    color: '#000000',
    fontFamily: 'Rajdhani',
  },

  text2: {
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Rajdhani',
    marginLeft: wp('10%'),
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  btn: {
    height: '8%',
    width: '80%',
    backgroundColor: '#4C2E84',
    alignSelf: 'center',
    borderRadius: 20,
    marginTop: 20,
  },
  btntxt: {
    fontSize: 20,
    padding: 15,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'Rajdhani',
  },

  inputbox: {
    alignSelf: 'center',
    margin: '5%',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    padding: 8,
    fontFamily: 'Rajdhani',
  },

  textinputbox: {height: 40, width: 300, backgroundColor: 'white'},
});
