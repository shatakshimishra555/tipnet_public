import * as React from 'react';
import {View, useWindowDimensions} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import FirstRoute from './TopTabscreen/firstrout';
import SecondRoute from './TopTabscreen/secondroute';
import ThirdRoute from './TopTabscreen/thirdroute';
import fourthRoute from './TopTabscreen/fourthroute';



const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
  third: ThirdRoute,
  fourth: fourthRoute,
});

export default function TopTabView() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'First'},
    {key: 'second', title: 'Second'},
    {key: 'third', title: 'third'},
    {key: 'fourth', title: 'fourth'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      style={{ marginTop:20}}
    />
  );
}
