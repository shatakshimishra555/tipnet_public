import React, {useState} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Pressable,
  Switch,
} from 'react-native';
import {Card, TextInput} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TopTabView from '../TopTabScreen';
import Slider from '@react-native-community/slider';

const Databse = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                elevation: 10,
                height: heightPercentageToDP('6%'),
                width: widthPercentageToDP('80%'),
                backgroundColor: '#F3F3F3',
                alignSelf: 'center',
                borderRadius: 10,
              }}>
              <View style={{flex: 5}}>
                <Text
                  style={{
                    color: 'black',
                    padding: heightPercentageToDP('1.5%'),
                    fontSize: heightPercentageToDP('2.3%'),
                    fontWeight: 'bold',
                  }}>
                  {' '}
                  shatakshi
                </Text>
              </View>
              <View style={{flex: 5}}>
                <Switch
                  trackColor={{false: '#767577', true: '#81b0ff'}}
                  thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitch}
                  value={isEnabled}
                  style={{marginTop: heightPercentageToDP('1%')}}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                height: heightPercentageToDP('6%'),
                width: widthPercentageToDP('80%'),
                backgroundColor: '#F3F3F3',
                alignSelf: 'center',
                borderRadius: 10,
                marginTop: heightPercentageToDP('2%'),
                elevation: 10,
              }}>
              <View style={{flex: 5}}>
                <Text
                  style={{
                    color: 'black',
                    padding: heightPercentageToDP('1.5%'),
                    fontSize: heightPercentageToDP('2.3%'),
                    fontWeight: 'bold',
                  }}>
                  {' '}
                  Bankroll 1
                </Text>
              </View>
              <View style={{flex: 5}}>
                <Entypo
                  name="chevron-down"
                  size={35}
                  color="black"
                  style={{alignSelf: 'flex-end', marginRight: 5}}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                height: heightPercentageToDP('6%'),
                width: widthPercentageToDP('80%'),
                backgroundColor: '#F3F3F3',
                alignSelf: 'center',
                borderRadius: 10,
                marginTop: heightPercentageToDP('2%'),
                elevation: 10,
              }}>
              <View style={{flex: 5}}>
                <Text
                  style={{
                    color: 'black',
                    padding: heightPercentageToDP('1.5%'),
                    fontSize: heightPercentageToDP('2.3%'),
                    fontWeight: 'bold',
                  }}>
                  {' '}
                  Bookmarker
                </Text>
              </View>
              <View style={{flex: 5}}>
                <Entypo
                  name="chevron-down"
                  size={35}
                  color="black"
                  style={{alignSelf: 'flex-end', marginRight: 5}}
                />
              </View>
            </View>

            <Card style={styles.cardview}>
              <Text style={styles.text1}>Football</Text>
            </Card>

            <Card style={styles.cardview1}>
              <Text style={styles.text1}>Tennis</Text>
            </Card>

            <Card style={styles.cardview1}>
              <Text style={styles.text1}>Basketball</Text>
            </Card>

            <Card style={styles.cardview1}>
              <Text style={styles.text1}>Baseball</Text>
            </Card>

            <Ionicons
              name="add-circle"
              size={35}
              color="#4F8EF7"
              style={{alignSelf: 'center', margin: heightPercentageToDP('3%')}}
            />

            <Text
              style={{
                fontSize: heightPercentageToDP('2.3%'),
                alignSelf: 'flex-start',
                color: 'black',
                fontWeight: 'bold',
              }}>
              Confiance
            </Text>
            <Slider
              style={{width: 320, height: 60}}
              minimumValue={0}
              maximumValue={1}
              minimumTrackTintColor="#FFFFFF"
              maximumTrackTintColor="#000000"
            />
{/* <View>  
<TextInput
        style={styles.input}
      
      
        placeholder="useless placeholder"
        keyboardType="numeric"
      />
            </View> */}
            <TouchableOpacity
              style={{
                marginTop: heightPercentageToDP('8%'),
                backgroundColor: '#4F8EF7',
                height: heightPercentageToDP('7%'),
                width: widthPercentageToDP('50%'),
                borderRadius:10

              }}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={{fontSize:20 , alignSelf:"center", marginTop:10 , color:"white" }}>Publier</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <View style={styles.headermainview}>
        <View style={{flex: 2}}>
          <TouchableOpacity>
            <Ionicons
              name="md-menu-sharp"
              size={35}
              color="#4F8EF7"
              style={styles.headericon1}
            />
          </TouchableOpacity>
        </View>

        <View style={{flex: 8}}>
          <Image
            source={require('../../Assets/img/tipnetlogo.png')}
            style={styles.imgheaderlogo}></Image>
        </View>
        <View style={{flex: 2}}>
          <TouchableOpacity>
            <AntDesign
              name="setting"
              size={35}
              color="#4F8EF7"
              style={styles.headericon2}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 2}}>
          <TouchableOpacity>
            <Ionicons
              name="notifications"
              size={35}
              color="#4F8EF7"
              style={styles.headericon2}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Image
        source={require('../../Assets/img/tiplog.png')}
        style={{
          alignSelf: 'center',
          marginTop: heightPercentageToDP('4%'),
          height: 100,
          width: 100,
        }}></Image>

      <View
        style={{
          flexDirection: 'row',
        }}>
        <View style={{flex: 3}}>
          <TouchableOpacity>
            <Text style={styles.text55}>140</Text>
          </TouchableOpacity>
          <Text style={styles.text555}>Bankrolls</Text>
        </View>

        <View style={{flex: 3}}>
          <TouchableOpacity>
            <Text style={styles.text55}>524</Text>
          </TouchableOpacity>
          <Text style={styles.text555}>Followers</Text>
        </View>

        <View style={{flex: 3}}>
          <TouchableOpacity>
            <Text style={styles.text55}>343</Text>
          </TouchableOpacity>
          <Text style={styles.text555}>Followers</Text>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => setModalVisible(true)}
        style={{
          height: '9%',
          width: '90%',
          alignSelf: 'center',
          borderRadius: 20,
          borderWidth: 1,
          borderRadius: 20,
          marginTop: heightPercentageToDP('3%'),
        }}>
        <Text
          style={{
            fontSize: 20,
            padding: 18,
            color: 'black',
            alignSelf: 'center',
            fontFamily: 'Rajdhani',
            fontWeight: 'bold',
          }}>
          Edit
        </Text>
      </TouchableOpacity>
      <TopTabView />
    </View>
  );
};
export default Databse;

const styles = StyleSheet.create({
  headericon1: {
    marginLeft: widthPercentageToDP('2%'),
    marginTop: heightPercentageToDP('0.5%'),
  },

  headericon2: {
    marginTop: heightPercentageToDP('0.5%'),
  },
  headermainview: {
    flexDirection: 'row',

    backgroundColor: 'white',
    paddingTop: heightPercentageToDP('3%'),
  },

  imgheaderlogo: {
    alignSelf: 'center',
    marginTop: 10,
    height: heightPercentageToDP('3.5%'),
    marginBottom: heightPercentageToDP('2%'),
  },

  text55: {
    marginTop: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('3%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
    alignSelf: 'center',
  },

  text555: {
    marginTop: heightPercentageToDP('1%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    color: 'gray',
    alignSelf: 'center',
  },
  modalView: {
    height: heightPercentageToDP('80%'),
    width: widthPercentageToDP('90%'),
    margin: 20,
    backgroundColor: '#D3D3D3',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },

  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

  cardview: {
    flexDirection: 'row',
    height: heightPercentageToDP('6%'),
    width: widthPercentageToDP('80%'),
    backgroundColor: '#F3F3F3',
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: heightPercentageToDP('2%'),
  },

  cardview1: {
    flexDirection: 'row',
    height: heightPercentageToDP('6%'),
    width: widthPercentageToDP('80%'),
    backgroundColor: '#F3F3F3',
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: -2,
  },

  text1: {
    color: 'black',
    padding: heightPercentageToDP('1.5%'),
    fontSize: heightPercentageToDP('2.3%'),
    fontWeight: 'bold',
  },

  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
