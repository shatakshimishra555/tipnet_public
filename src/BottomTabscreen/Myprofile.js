import React, {useState} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Card} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import * as Progress from 'react-native-progress';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useNavigation} from '@react-navigation/native';
import DrawerNavigationScreen from '../drawernavigation';

const navigation = useNavigation();


 const Myprofile =() => {
  return (
    <View
      style={{
        flex: 1,
      }}>
      <ScrollView>
        <View style={styles.headermainview}>
          <View style={{flex: 2}}>
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Ionicons
                name="md-menu-sharp"
                size={35}
                color="#4F8EF7"
                style={styles.headericon1}
              />
            </TouchableOpacity>
          </View>

          <View style={{flex: 8}}>
            <Image
              source={require('../../Assets/img/tipnetlogo.png')}
              style={styles.imgheaderlogo}></Image>
          </View>
          <View style={{flex: 2}}>
            <TouchableOpacity>
              <AntDesign
                name="setting"
                size={35}
                color="#4F8EF7"
                style={styles.headericon2}
              />
            </TouchableOpacity>
          </View>
          <View style={{flex: 2}}>
            <TouchableOpacity>
              <Ionicons
                name="notifications"
                size={35}
                color="#4F8EF7"
                style={styles.headericon2}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Card style={{flex: 1, marginTop: heightPercentageToDP('2%')}}>
          <Image
            source={require('../../Assets/img/graph.png')}
            style={styles.img2}></Image>
        </Card>
        <Text style={styles.Text3}>All Updates</Text>
        <Card style={{flex: 1}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.img3}></Image>
            </View>
            <View style={{flex: 9}}>
              <Text style={styles.text4}>rajdhani</Text>

              <Text style={styles.text5}>8 minute ago</Text>
            </View>
          </View>

          <Text style={styles.text6}>
            Kobe’s passing is really sticking w/ me in a way I didn’t expect.
          </Text>
        </Card>
        <Card style={{flex: 1}}>
          <Image
            source={require('../../Assets/img/graph.png')}
            style={styles.img2}></Image>

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: widthPercentageToDP('8%'),
            }}>
            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <FontAwesome5
                  name="comment-alt"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>46</Text>
            </View>

            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <Feather
                  name="share"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>18</Text>
            </View>

            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <AntDesign
                  name="heart"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>363</Text>
            </View>
          </View>
        </Card>
        <Card style={{flex: 1, marginTop: 20, marginBottom: 20}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <Image
                source={require('../../Assets/img/profilepic.png')}
                style={styles.profilepic}></Image>
            </View>
            <View style={{flex: 8}}>
              <Text style={styles.text7}>rajdhani</Text>

              <Text style={styles.text8}>8 minute ago</Text>
            </View>

            <View style={{flex: 2}}>
              <Text style={styles.text9}>Public</Text>
            </View>
          </View>

          <Text style={styles.text10}>Bankroll_name</Text>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 3}}>
              <Text style={styles.Text1}>Om-PSG</Text>
            </View>

            <View style={{flex: 3}}>
              <Text style={styles.Text1}>Victoire VSG</Text>
            </View>

            <View style={{flex: 3}}>
              <Text style={styles.Text1}>1,38</Text>
            </View>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: widthPercentageToDP('10%'),
            }}>
            <View style={{flex: 2}}>
              <Text style={styles.Text1}>Confiance:</Text>
            </View>

            <View style={{flex: 8}}>
              <Progress.Bar
                progress={0.3}
                width={200}
                style={styles.Progressbar}
              />
            </View>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: widthPercentageToDP('10%'),
            }}>
            <View style={{flex: 2}}>
              <Text style={styles.Text1}>Analysis:</Text>
            </View>

            <View style={{flex: 8}}>
              <Text style={styles.Text2}> Le Lorem Ipsum est simplement</Text>
            </View>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: widthPercentageToDP('8%'),
              paddingTop: heightPercentageToDP('2%'),
            }}>
            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <FontAwesome5
                  name="comment-alt"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>46</Text>
            </View>

            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <Feather
                  name="share"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>18</Text>
            </View>

            <View style={{flex: 3, flexDirection: 'row'}}>
              <TouchableOpacity>
                <AntDesign
                  name="heart"
                  size={20}
                  color="#4F8EF7"
                  style={styles.commenticon}
                />
              </TouchableOpacity>
              <Text style={styles.text55}>363</Text>
            </View>
          </View>
        </Card>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  Text1: {
    alignSelf: 'center',
    marginTop: heightPercentageToDP('4%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  Text2: {
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    marginLeft: 5,
    color: 'gray',
    marginTop: heightPercentageToDP('4%'),
  },

  headericon1: {
    marginLeft: widthPercentageToDP('2%'),
    marginTop: heightPercentageToDP('0.5%'),
  },

  headericon2: {
    marginTop: heightPercentageToDP('0.5%'),
  },
  headermainview: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: 'white',
    paddingTop: heightPercentageToDP('3%'),
  },

  imgheaderlogo: {
    alignSelf: 'center',
    marginTop: 10,
    height: heightPercentageToDP('3.5%'),
    marginBottom: heightPercentageToDP('2%'),
  },

  img2: {
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 20,
    marginBottom: heightPercentageToDP('2%'),
  },

  Text3: {
    marginLeft: heightPercentageToDP('4%'),
    marginTop: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2.8%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  img3: {
    alignSelf: 'center',
    marginTop: 10,
    height: '90%',
    width: '100%',
    // overflow: 'hidden',
    borderRadius: 20,
    marginBottom: heightPercentageToDP('2%'),
  },

  text4: {
    marginLeft: heightPercentageToDP('2%'),
    marginTop: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2.8%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  text5: {
    marginLeft: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'gray',
  },

  text6: {
    alignSelf: 'center',
    marginTop: heightPercentageToDP('4%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    margin: heightPercentageToDP('3%'),
    color: 'black',
  },

  profilepic: {
    alignSelf: 'center',
    marginTop: 10,
    height: '100%',
    width: '100%',
    borderRadius: 20,
    marginBottom: heightPercentageToDP('2%'),
  },
  text7: {
    marginLeft: heightPercentageToDP('2%'),
    marginTop: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2.8%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'black',
  },

  text8: {
    marginLeft: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'gray',
  },

  text9: {
    marginTop: heightPercentageToDP('3.9%'),
    fontSize: heightPercentageToDP('2.3%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'blue',
  },

  text10: {
    alignSelf: 'flex-end',
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    marginRight: heightPercentageToDP('1%'),
    color: 'gray',
  },

  Progressbar: {
    marginTop: heightPercentageToDP('5%'),
  },

  commenticon: {
    alignSelf: 'center',
    padding: heightPercentageToDP('2%'),
  },

  text55: {
    marginTop: heightPercentageToDP('2%'),
    fontSize: heightPercentageToDP('2%'),
    fontFamily: 'rajdhani',
    fontWeight: 'bold',
    color: 'gray',
  },
});


export default Myprofile;