import React from 'react';
import { Text, View , StyleSheet} from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { heightPercentageToDP } from 'react-native-responsive-screen';

const Mypro = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}>
      <Text>Myprofile</Text>
    </View>
  )
}
export default Mypro;
