import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const Otpinput = props => {
  const navigation = useNavigation();

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <Image
        source={require('../Assets/img/Tipnet.png')}
        style={{alignSelf: 'center', marginTop: '20%'}}></Image>

      <Card
        style={{
          borderRadius: 30,
          height: '60%',
          width: '100%',
          alignSelf: 'center',
          marginTop: '40%',
          backgroundColor: '#4958FF',
        }}>
        <Text
          style={{
            fontSize: hp('3%'),
            color: 'white',
            marginTop: hp('5%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
            fontWeight: 'bold',
          }}>
          CODE VERIFICATION
        </Text>

        <Text
          style={{
            fontSize: hp('2.3%'),
            color: 'white',
            marginTop: hp('3%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Entrez le code envoyé à {'\n'}user@email.com
        </Text>

        <View
          style={{
            height: hp('3%'),
            width: wp('88% '),
            justifyContent: 'center',
            alignSelf: 'center',
            flexDirection: 'row',
            flex: 1,
            marginTop: hp('5%'),
          }}>
          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>

          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              maxFontSizeMultiplier={1}
              style={styles.TextInput}
              placeholder="0"></TextInput>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => props.navigation.navigate('Newpasswordrest')}
          style={{
            height: '12%',
            width: '80%',
            backgroundColor: '#00EE6D',
            alignSelf: 'center',
            marginTop: -300,
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: hp('2%'),
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
            }}>
            {' '}
            Verifier
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}
          style={{
            height: '12%',
            width: '80%',
            alignSelf: 'center',
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: 15,
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight: 'bold',
            }}>
            {' '}
            Renvoyer le code
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  );
};

export default Otpinput;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('8%'),
    height: '20%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: '#21899C',
  },

  TextInput: {
    height: 40,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    alignSelf: 'center',
  },
});
