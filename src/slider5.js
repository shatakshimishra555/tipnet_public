import React from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Sliderscreen5 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.maincontainer}>
      <Image
        source={require('../Assets/img/tipnetlogo.png')}
        style={styles.img1}></Image>

      <TouchableOpacity
        onPress={() => navigation.navigate('ResetPass')}
        style={{height: hp('40%')}}>
        <Image
          source={require('../Assets/img/greentick1.png')}
          style={styles.img2}></Image>
      </TouchableOpacity>
      <Text style={styles.text1}>Votre inscription est réussi </Text>

      <Image
        source={require('../Assets/img/appslider1.png')}
        style={styles.img3}></Image>
    </View>
  );
};

export default Sliderscreen5;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('7%'),
    height: '60%',
    width: '60%',
  },

  img3: {
    alignSelf: 'center',
    marginTop: '20%',
    height: '5%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },
});
