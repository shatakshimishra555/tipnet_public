import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const Newpasswordrest = props => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <Image
        source={require('../Assets/img/Tipnet.png')}
        style={{alignSelf: 'center', marginTop: '40%'}}></Image>

      <Card
        style={{
          borderRadius: 30,
          height: '70%',
          width: '100%',
          alignSelf: 'center',
          marginTop: '15%',
          backgroundColor: '#4958FF',
        }}>
        <Text
          style={{
            fontSize: hp('3%'),
            color: 'white',
            marginTop: hp('5%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          MOT DE PASSE OUBLIÉ
        </Text>

        <Text
          style={{
            fontSize: hp('2.5%'),
            color: 'white',
            marginTop: hp('8%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Password
        </Text>

        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          type="email"
          onChangeText={val => textInputChange(val)}
          onEndEditing={e => handleValidUser(e.nativeEvent.text)}
          style={{
            height: '10%',
            width: '80%',
            backgroundColor: 'white',
            marginTop: hp('1%'),
            marginLeft: hp('5%'),
          }}
          placeholder="*****"
        />

        <Text
          style={{
            fontSize: hp('2.5%'),
            color: 'white',
            marginTop: hp('2%'),
            marginLeft: hp('5%'),
            fontFamily: 'Rajdhani',
          }}>
          Re-type Password
        </Text>

        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          type="email"
          onChangeText={val => textInputChange(val)}
          onEndEditing={e => handleValidUser(e.nativeEvent.text)}
          style={{
            height: '10%',
            width: '80%',
            backgroundColor: 'white',
            marginTop: hp('1%'),
            marginLeft: hp('5%'),
          }}
          placeholder="*******"
        />

        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}

          style={{
            height: '10%',
            width: '80%',
            backgroundColor: '#00EE6D',
            alignSelf: 'center',
            marginTop: hp('4%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: hp('2%'),
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
            }}>
            {' '}
            Envoyer
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          // onPress={() => navigation.navigate('Details')}
          style={{
            height: '10%',
            width: '80%',

            alignSelf: 'center',
            marginTop: hp('2%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: 15,
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight: 'bold',
            }}>
            {' '}
            Cancel
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  );
};

export default Newpasswordrest;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('8%'),
    height: '20%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: '#21899C',
  },
});
