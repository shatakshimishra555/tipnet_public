import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const Updatepasswordsuccessfull = props => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <Image
        source={require('../Assets/img/Tipnet.png')}
        style={{alignSelf: 'center', marginTop: hp('40%')}}></Image>

      <Card
        style={{
          borderRadius: 30,
          height: '58%',
          width: '100%',
          alignSelf: 'center',
          marginTop: hp('40%'),
          backgroundColor: '#4958FF',
        }}>
        <Text
          style={{
            fontSize: hp('3%'),
            color: 'white',
            marginTop: hp('5%'),
            alignSelf: 'center',
            fontFamily: 'Rajdhani',
            fontWeight: 'bold',
          }}>
          MOT DE PASSE MIS A JOUR
        </Text>

        <Text
          style={{
            fontSize: hp('2%'),
            color: 'white',
            marginTop: hp('3%'),

            alignSelf: 'center',
            fontFamily: 'Rajdhani',
          }}>
          Votre mot de passe a été mis à jour
        </Text>

        <TouchableOpacity
          onPress={() => props.navigation.navigate('Updatepasswordsuccessfull')}
          style={{
            height: '12%',
            width: '80%',
            backgroundColor: '#00EE6D',
            alignSelf: 'center',
            marginTop: hp('6%'),
          }}>
          <Text
            style={{
              fontSize: hp('2.5%'),
              padding: hp('2%'),
              color: 'white',
              alignSelf: 'center',
              fontFamily: 'Rajdhani',
              fontWeight:"bold"
            }}>
            {' '}
            Login
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  );
};

export default Updatepasswordsuccessfull;

const styles = StyleSheet.create({
  img1: {
    alignSelf: 'center',
    marginTop: '30%',
    height: '15%',
    width: '70%',
  },

  img2: {
    alignSelf: 'center',
    marginTop: hp('8%'),
    height: '20%',
    width: '50%',
  },

  text1: {
    fontSize: hp('2.5%'),
    fontWeight: '600',
    marginTop: hp('7%'),
    alignSelf: 'center',
    color: '#969AA',
    fontFamily: 'Rajdhani',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: '#21899C',
  },
});
