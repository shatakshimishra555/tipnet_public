import * as React from 'react';
import {Button, View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './src/Home';
import DetailsScreen from './src/detail';
import Sliderscreen1 from './src/slider1';
import Sliderscreen2 from './src/slider2';
import Sliderscreen3 from './src/slider3';
import Sliderscreen4 from './src/slider4';
import Sliderscreen5 from './src/slider5';
import ResetPassword from './src/resetpassword';
import RegisterUser from './src/registeruser';
import Resendverificationcode from './src/resendverificationcode';
import Updatepasswordsuccessfull from './src/updatepassword';
import Otpinput from './src/Otpinput';
import Newpasswordrest from './src/newpasswordreset';
import BottomTab from './src/Bottomtab';
import TopTabView from './src/TopTabScreen';
import Modal1 from './src/Modalscreens/Modal1';
import DrawerNavigationScreen from './src/drawernavigation';

import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Details"
          component={DetailsScreen}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Slider1"
          component={Sliderscreen1}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Slider2"
          component={Sliderscreen2}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Slider3"
          component={Sliderscreen3}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Slider4"
          component={Sliderscreen4}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Slider5"
          component={Sliderscreen5}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="ResetPass"
          component={ResetPassword}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Register"
          component={RegisterUser}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="resendverificationcode"
          component={Resendverificationcode}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Otpinput"
          component={Otpinput}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Updatepasswordsuccessfull"
          component={Updatepasswordsuccessfull}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Newpasswordrest"
          component={Newpasswordrest}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="BottomTab"
          component={BottomTab}
          screenOptions={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="TopTabView"
          component={TopTabView}
          screenOptions={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="modal1"
          component={Modal1}
          screenOptions={{
            headerShown: false,
          }}
        />
         <Stack.Screen
          name="DrawerScreen"
          component={DrawerNavigationScreen}
          screenOptions={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
